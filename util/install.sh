#!/bin/sh
# $Diogenes: install.sh 0.1 2020/05/17 7:00:00 geoghegan $

# Copyright (c) 2020 Jordan Geoghegan <jordan@geoghegan.ca>

# Permission to use, copy, modify, and/or distribute this software for any 
# purpose with or without fee is hereby granted, provided that the above 
# copyright notice and this permission notice appear in all copies.

# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH 
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM 
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE 
# OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR 
# PERFORMANCE OF THIS SOFTWARE.

# Global variables
web_dir="/var/www/dio"

#####################################################################
# Messages
#####################################################################

banner () {
	printf "\n\n========================================================="
	printf "\n== DiogenesCCTV : Ever Searching for an Honest Man ==\n"
	printf "=========================================================\n\n"
}

#####################################################################
# Abort Sequences
#####################################################################

preinstall_abort () {
	printf "\n\n!!! Failed to perform pre-install tweaks to settings!\nExiting...\n\n" ; exit 1
}

root_perms_abort () {
	# Error prints correct message based on "$getroot" variable
	# Error checks if running on a platform that uses "doas" (Void Linux, OpenBSD) or sudo (Generic Linux etc)
	printf "\n\n!!! %s is not enabled! Please check /etc/%s.conf configuration.\nExiting...\n\n" "$getroot" "$getroot" ; exit 1
}

package_install_abort () {
	printf "\n\n!!! Package Install Failed!\nExiting...\n\n" ; exit 1
}

user_create_abort () {
	while true ; do
	printf '\n\n!!! Creation of system user "_diogenes" failed. !!!\n'
	printf '\nIf DiogenesCCTV was previously installed on this machine, the account "_diogenes" may still exist\n'
	printf '\nPlease exit now and delete the user and its home folder and rerun this script\n'
	printf '\nOtherwise, if you know what you are doing, you can choose to ignore this warning and proceed anyways\n\n'
	printf "(Exit or Proceed?) : "
	    read -r user_yn
	    case $user_yn in
	        [Ee]* ) echo "Exiting..." ; exit 0;;
	        [Pp]* ) echo "Proceeding..." ; break;;
	        * ) echo 'Please answer "E" to Exit, or "P" to Proceed.';;
	    esac
	done
}

source_dl_abort () {
	printf "\n\n!!! Failed to download DiogenesCCTV source code from GitLab. Please check your internet connection.\nExiting...\n\n" ; exit 1
}

source_fetch_abort () {
	printf "\n\n!!! Failed to fetch DiogenesCCTV source code!\nExiting...\n\n" ; exit 1
}

go_dependency_fetch_abort () {
	printf "\n\n!!! Failed to install DiogenesCCTV dependencies!\nExiting...\n\n" ; exit 1
}

go_get_abort () {
	printf "\n\nFailed to update DiogenesCCTV Golang dependencies!\nExiting...\n\n" ; exit 1
}

#####################################################################
# Platform Agnostic Yes/No Functions
#####################################################################

os_choice () {
	# Platform Choice
	while true ; do
	printf "\nWhich Operating System are you installing DiogenesCCTV on?\n"
	#printf "[A]lpine Linux, [C]entOS, [F]reeBSD, [M]acOS, [O]penBSD, [R]asbian or [U]buntu\n\n"
	printf "[A]lpine Linux / [O]penBSD\n\n"
	printf "Which OS? : "
	    read -r oschoice
	    case $oschoice in
                [Aa]* ) alpine_install  ; break;;
#                [Cc]* ) centos_install  ; break;;
#                [Ff]* ) freebsd_install ; break;;
#                [Mm]* ) macos_install   ; break;;
                [Oo]* ) openbsd_install ; break;;
#                [Rr]* ) rasbian_install ; break;;
#                [Uu]* ) ubuntu_install  ; break;;
	        * ) echo 'Please select which Operating System you are using';;
	    esac
	done
}

am_root () {
	"$getroot" whoami >/dev/null 2>&1 || root_perms_abort
}

#####################################################################
# Platform Agnostic Functions
# Make sure to set the "$getroot" and "$getnet" variables in the root function of your platforms installer
#####################################################################

my_ipv4 () {
    ifconfig | awk '{gsub("/[0-9]?[0-9]$","",$2)}; {gsub("addr:","",$2)}; /inet / {if ($1="inet" && $2!="127\.0\.0\.1") {print $2}}' | head -n1
}

source_fetch () {
	# Variables
	src_tmpfile="$($getroot -u _diogenes mktemp)"
	src_tmpdir="$("$getroot" -u _diogenes mktemp -d)"

	# Download Source
	printf "\n### Downloading DiogenesCCTV source from master branch\n\n"
	"$getroot" -u _diogenes /bin/sh -c "$getnet $src_tmpfile https://gitlab.com/JordanGeoghegan/DiogenesCCTV/-/archive/master/DiogenesCCTV-master.tar.gz" || source_fetch_abort

	printf "\n### Extracting to install directory\n\n"
	"$getroot" -u _diogenes tar -xzf "$src_tmpfile" -C "$src_tmpdir" || source_fetch_abort
	"$getroot" -u _diogenes rsync -a --checksum "$src_tmpdir"/DiogenesCCTV-master/src/go/ /home/_diogenes/go/ || source_fetch_abort
	"$getroot" -u _diogenes rsync -a --checksum "$src_tmpdir"/DiogenesCCTV-master/src/sh/ /home/_diogenes/sh/ || source_fetch_abort
	"$getroot" -u _diogenes rsync -a --checksum "$src_tmpdir"/DiogenesCCTV-master/util/ /home/_diogenes/util/ || source_fetch_abort
	"$getroot" -u _diogenes rsync -a --checksum "$src_tmpdir"/DiogenesCCTV-master/src/conf/ /home/_diogenes/conf/ || source_fetch_abort

	# (Caddy) Try to "Do the right thing(tm)" wrt config file handling
	# We don't use Caddy on OpenBSD...
	if [ "$ostype" != "openbsd" ]; then
		# Make sure we dont overwrite peoples config files
		if stat /etc/caddy/caddy.conf > /dev/null 2>&1 ; then
			# If caddy.conf file hashes are identical, no action is required
			if [ "$($hashcheck < "$src_tmpdir"/DiogenesCCTV-master/src/conf/caddy.conf)" = "$($hashcheck < /etc/caddy/caddy.conf)" ]; then
        			echo "No new changes in caddy.conf..."
			else	
				echo "Changes have been made to the default DiogenesCCTV caddy.conf template"
				while true ; do
					printf "Would you like to install the recommended Caddy config now, or manually merge the changes later?\n"
					printf "Enter \"yes\" to install now, or \"no\" if you want to do it manually later.\n"
					printf "(Yes/no) : "
    	    				read -r caddy_yn
    	    				case $caddy_yn in
        					[Yy]* ) echo "Overwriting /etc/caddy.conf with new default config..." && printf "\n\nIf you are updating DiogenesCCTV to a new version, you will need to restart Caddy (or the machine) for the new config file to be loaded\n\n" ; install -m 644 -o root -g root "$src_tmpdir"/DiogenesCCTV-master/src/conf/caddy.conf /etc/caddy/caddy.conf || source_fetch_abort ; break;;
        					[Nn]* ) echo "Updated default template can be found at \"/etc/caddy/caddy.conf.NEW\"" ; echo "You will have to merge any changes manually" ; install -m 644 -o root -g root "$src_tmpdir"/DiogenesCCTV-master/src/conf/caddy.conf /etc/caddy/caddy.conf.NEW || source_fetch_abort ; break;;
        					* ) echo "Please answer yes or no.";;
    	    				esac
				done
			fi
		else
    			printf "\n### Installing caddy.conf\n\n"
			install -m 644 -o root -g root "$src_tmpdir"/DiogenesCCTV-master/src/conf/caddy.conf /etc/caddy/caddy.conf || source_fetch_abort
		fi
	fi

	# (relayd) Try to "Do the right thing(tm)" wrt config file handling
	if [ "$ostype" = "openbsd" ]; then
		# Make sure we dont overwrite peoples config files
		if stat /etc/relayd.conf > /dev/null 2>&1 ; then
			# If relayd.conf file hashes are identical, no action is required
			if [ "$($hashcheck < "$src_tmpdir"/DiogenesCCTV-master/src/conf/relayd.conf)" = "$($hashcheck < /etc/relayd.conf)" ]; then
        			echo "No new changes in relayd.conf..."
			else	
				printf "\nChanges have been made to the default DiogenesCCTV relayd.conf template\n\n"
				while true ; do
					printf "Would you like to install the recommended relayd config now, or manually merge the changes later?\n\n"
					printf "Enter \"yes\" to install now, or \"no\" if you want to do it manually later.\n"
					printf "(Yes/no) : "
    	    				read -r relayd_yn
    	    				case $relayd_yn in
        					[Yy]* ) printf "\nOverwriting /etc/relayd.conf with new default config...\n\n" && install -m 600 -o root -g wheel "$src_tmpdir"/DiogenesCCTV-master/src/conf/relayd.conf /etc/relayd.conf || source_fetch_abort && rcctl reload relayd  ; break;;
        					[Nn]* ) printf "\nUpdated default template can be found at \"/etc/relayd.conf.NEW\"\n\n" && printf "\nYou will have to merge any changes manually\n\n" ; install -m 600 -o root -g wheel "$src_tmpdir"/DiogenesCCTV-master/src/conf/relayd.conf /etc/relayd.conf.NEW || source_fetch_abort ; break;;
        					* ) echo "Please answer yes or no.";;
    	    				esac
				done

			fi
		else
    			printf "\n### Installing relayd.conf\n\n"
			install -m 600 -o root -g wheel "$src_tmpdir"/DiogenesCCTV-master/src/conf/relayd.conf /etc/relayd.conf || source_fetch_abort
			rcctl enable relayd && rcctl start relayd
		fi
	fi

	# (httpd) Try to "Do the right thing(tm)" wrt config file handling
	if [ "$ostype" = "openbsd" ]; then
		# Make sure we dont overwrite peoples config files
		if stat /etc/httpd.conf > /dev/null 2>&1 ; then
			# If httpd.conf file hashes are identical, no action is required
			if [ "$($hashcheck < "$src_tmpdir"/DiogenesCCTV-master/src/conf/httpd.conf)" = "$($hashcheck < /etc/httpd.conf)" ]; then
        			echo "No new changes in httpd.conf..."
			else	
				printf "\nChanges have been made to the default DiogenesCCTV httpd.conf template\n\n"
				while true ; do
					printf "Would you like to install the recommended httpd config now, or manually merge the changes later?\n\n"
					printf "Enter \"yes\" to install now, or \"no\" if you want to do it manually later.\n"
					printf "(Yes/no) : "
    	    				read -r httpd_yn
    	    				case $httpd_yn in
        					[Yy]* ) printf "\nOverwriting /etc/httpd.conf with new default config...\n\n" && install -m 644 -o root -g wheel "$src_tmpdir"/DiogenesCCTV-master/src/conf/httpd.conf /etc/httpd.conf || source_fetch_abort && rcctl reload httpd ; break;;
        					[Nn]* ) printf "\nUpdated default template can be found at \"/etc/httpd.conf.NEW\"\n\n" && printf "\nYou will have to merge any changes manually\n\n" ; install -m 644 -o root -g wheel "$src_tmpdir"/DiogenesCCTV-master/src/conf/httpd.conf /etc/httpd.conf.NEW || source_fetch_abort ; break;;
        					* ) echo "Please answer yes or no.";;
    	    				esac
				done
			fi
		else
    			printf "\n### Installing httpd.conf\n\n"
			install -m 644 -o root -g wheel "$src_tmpdir"/DiogenesCCTV-master/src/conf/httpd.conf /etc/httpd.conf || source_fetch_abort
			rcctl enable httpd && rcctl start httpd
		fi
	fi

	printf "\n### Installing DiogenesCCTV web interface to \"%s\"\n\n" "$web_dir"
	mkdir -p "$web_dir" || source_fetch_abort
	chown _diogenes:_diogenes "$web_dir" || source_fetch_abort
	chmod 775 "$web_dir" || source_fetch_abort
	rsync -a --checksum --exclude "$web_dir"/video-recordings --exclude "$web_dir"/hls "$src_tmpdir"/DiogenesCCTV-master/src/www/ "$web_dir"/ || source_fetch_abort
	mkdir -p "$web_dir"/video-recordings || source_fetch_abort
	find "$web_dir" -exec chown _diogenes:_diogenes {} + || source_fetch_abort
	find "$web_dir" -type f -not -path "$web_dir/vendor/*" -exec chmod 664 {} + || source_fetch_abort
	find "$web_dir" -type d -not -path "$web_dir/vendor/*" -exec chmod 775 {} + || source_fetch_abort
	find "$web_dir"/vendor -type f -exec chmod 444 {} + || source_fetch_abort
	find "$web_dir"/vendor -type d -exec chmod 555 {} + || source_fetch_abort

	
	# If HLS symlink to /dev/shm doesn't exist, create it
	if [ "$ostype" = "linux" ]; then
		if stat "$web_dir"/hls > /dev/null 2>&1 ; then
			printf "\nHLS symlink exists, no action required. Proceeding...\n\n"
		else
			ln -s /dev/shm "$web_dir"/hls
		fi
	fi

	# Clean up after ourselves
	rm -rf "$src_tmpdir" "$src_tmpfile"
}

go_dependency_fetch () {
	printf "\n### Installing Golang Dependencies...\n\n"
	cd /home/_diogenes && "$getroot" -u _diogenes go get github.com/spf13/viper
}

#####################################################################
# Platform Install Scripts
#####################################################################


#####################################################################
# Alpine Linux Functions
#####################################################################

alpine_configure () {
	# Enable community repo
	sed -i '3 s/^#//g' /etc/apk/repositories || echo "Failed to enable community repository"
	# Update Packages
	apk update  || package_install_abort
	apk upgrade || package_install_abort
	apk add sudo || package_install_abort
	# Confirm sudo is working
	"$getroot" whoami >/dev/null 2>&1 || root_perms_abort
}

#####################################################################
# Alpine Linux Installer
#####################################################################

alpine_install () {
	# Set platform variables ("sudo" and "wget")
	getroot="sudo"
	getnet="wget -O"
	hashcheck="sha256sum"
 
	# Print welcome message and banner
	banner
	echo " "
	echo "Welcome to the DiogenesCCTV Alpine Linux Installer!"
	echo " "
	echo "Please follow the on screen instructions to install DiogenesCCTV and configure its dependencies"
	echo " "
	echo "To install DiogenesCCTV, we need to tweak some settings"
	echo " "
	echo "In order to install the required dependencies we need to ensure sudo is installed and enable the community package repository"
	echo " "

	while true ; do
	printf "Would you like to perfom the required settings tweaks?\n"
	printf "Enter \"yes\" unless you know what you're doing\n"
	printf "(Yes/no) : "
    	    read -r tweaks_yn
    	    case $tweaks_yn in
        	[Yy]* ) alpine_configure || preinstall_abort; break;;
        	[Nn]* ) echo "Proceeding..."; break;;
        	* ) echo "Please answer yes or no.";;
    	    esac
	done

	# Run standard root check function
	am_root

	while true ; do
	printf "\nThe following packages are required:\n\n  FFmpeg\n  Go\n  Caddy\n  netcat-openbsd\n  Git\n  Rsync\n"
	printf "\nWould you like to install them now?\n\n"
	printf "(Yes/no) : "
	    read -r package_deps
	    case $package_deps in
	        [Yy]* ) apk add ffmpeg go caddy netcat-openbsd git rsync || package_install_abort ; break;;
	        [Nn]* ) printf "Proceeding without package installation, HERE BE DRAGONS\n\n" ; break;;
	        * ) echo "Please answer yes or no.";;
	    esac
	done

	# DiogenesCCTV unpriv user creation
	printf '\n### Creating "_diogenes" System User\n\n'
	adduser -D -s /sbin/nologin -h /home/_diogenes _diogenes || user_create_abort

	# Delete Alpine's placeholder caddy.conf to avoid confusing prompt when installing for first time
	if stat /etc/caddy/caddy.conf > /dev/null 2>&1 ; then
		if [ "$(grep -cv '#' /etc/caddy/caddy.conf)" -le 2 ]; then
			rm -f /etc/caddy/caddy.conf
		fi
	fi

	# Run standard source_fetch function 
	source_fetch

	# Enable + Start Caddy
	echo "Configuring Caddy webserver to start at boot..."
	rc-update add caddy || echo "Failed to enable Caddy!"
	echo "Starting Caddy webserver..."
	rc-service caddy start || echo "Failed to start Caddy!"

	# Install Go dependencies
	go_dependency_fetch || go_dependency_fetch_abort

	# Move diogenes.conf to /etc/
	install -m 644 -o _diogenes -g _diogenes /home/_diogenes/conf/diogenes.conf /etc/diogenes.conf

	# Post-Install Info
	printf "\n\nCongratulations, DiogenesCCTV is now installed!\n\n"
	
	printf '\nTo start DiogenesCCTV at boot, add a crontab entry for the user "_diogenes" with something like this:\n\n'

	printf '# sudo crontab -u _diogenes -e
 
	TO BE DETERMINED'

	printf "\n\n\nYou can access DiogenesCCTV at http://%s:8000" "$(my_ipv4)"

	printf "\n\nThe official DiogenesCCTV documentation can be found at: TO BE DETERMINED\n\n"
}

#####################################################################
# CentOS Functions
#####################################################################

#####################################################################
# CentOS Installer
#####################################################################

centos_install () {
	echo "CentOS support has not been added yet" ; exit 0
}

#####################################################################
# FreeBSD Functions
#####################################################################

#####################################################################
# FreeBSD Installer
#####################################################################

freebsd_install () {
	echo "FreeBSD support has not been added yet" ; exit 0
}

#####################################################################
# MacOS Functions
#####################################################################

#####################################################################
# MacOS Installer
#####################################################################

macos_install () {
	echo "MacOS support has not been added yet" ; exit 0
}

#####################################################################
# OpenBSD Functions
#####################################################################

#####################################################################
# OpenBSD Installer
#####################################################################

openbsd_install () {
	# Set platform variables ("doas" and "ftp")
	ostype="openbsd"
	getroot="doas"
	getnet="ftp -o"
	hashcheck="sha512"
	
	# Print welcome message and banner
	banner
	echo " "
	echo "Welcome to the DiogenesCCTV OpenBSD Installer!"
	echo "Please follow the on screen instructions to install DiogenesCCTV and configure its dependencies"
	echo " "

	# allow root to call doas
	while true ; do
	printf "\nThis script requires the \"doas\" utility which is disabled by default on OpenBSD.\n"
	printf "\n\"permit nopass root\" must be added to the config file to enable doas for the root user\n"
	printf "\n\nWould you like to perform this action now?\n"
	printf "\nEnter \"yes\" unless you have previously enabled doas for the root user\n\n"
	printf "(Yes/no) : "
	    read -r doas_root
	    case $doas_root in
	        [Yy]* ) echo "permit nopass root" >> /etc/doas.conf && echo "Success!" ; break;;
	        [Nn]* ) printf "Proceeding...\n\n"  ; break;;
	        * ) echo "Please answer yes or no.";;
	    esac
	done

	# Run standard root check function
	am_root	

	while true ; do
	printf "\nThe following packages are required:\n\n  FFmpeg\n  Go\n  Git\n  Rsync\n"
	printf "\nWould you like to install them now?\n\n"
	printf "(Yes/no) : "
	    read -r package_deps
	    case $package_deps in
	        [Yy]* ) pkg_add ffmpeg go git rsync-- || package_install_abort ; break;;
	        [Nn]* ) printf "Proceeding without package installation, HERE BE DRAGONS\n\n"  ; break;;
	        * ) echo "Please answer yes or no.";;
	    esac
	done

	# DiogenesCCTV unpriv user creation
	printf '\n### Creating "_diogenes" System User\n\n'
	useradd -s /sbin/nologin -m -d /home/_diogenes _diogenes || user_create_abort

	# Run standard source_fetch function 
	source_fetch

	# Install Go dependencies
	go_dependency_fetch || go_dependency_fetch_abort

	# Move diogenes.conf to /etc/
	install -m 644 -o _diogenes -g _diogenes /home/_diogenes/conf/diogenes.conf /etc/diogenes.conf

	# Post-Install Info
	printf "\n\nCongratulations, DiogenesCCTV is now installed!\n\n"

	printf '\nTo start DiogenesCCTV at boot, add a crontab entry for the user "_diogenes" with something like this:\n\n'

	printf '$ doas crontab -u _diogenes -e
 
	TO BE DETERMINED'

	printf "\n\n\nYou can access DiogenesCCTV at http://%s:8000" "$(my_ipv4)"
	
	printf "\n\nThe official DiogenesCCTV documentation can be found at: TO BE DETERMINED\n\n"

}

#####################################################################
# Rasbian Functions
#####################################################################

#####################################################################
# Rasbian Installer
#####################################################################

rasbian_install () {
	echo "Rasbian support has not been added yet" ; exit 0
}

#####################################################################
# Ubuntu Functions
#####################################################################

#####################################################################
# Ubuntu Installer
#####################################################################

ubuntu_install () {
	echo "Ubuntu support has not been added yet" ; exit 0
}

#####################################################################
# DiogenesCCTV code updater
#####################################################################

update () {
	# Platform Choice
	while true ; do
	printf "\nWhich Operating System is DiogenesCCTV currently installed on?\n"
	#printf "[A]lpine Linux, [C]entOS, [F]reeBSD, [M]acOS, [O]penBSD, [R]asbian or [U]buntu\n\n"
	printf "[A]lpine Linux / [O]penBSD\n\n"
	printf "Which OS? : "
	    read -r oschoice
	    case $oschoice in
                [Aa]* ) ostype="linux" ; getroot="sudo" ; getnet="wget -O" ; hashcheck="sha256sum" ; break;;
#                [Cc]* ) centos_install   ; break;;
#                [Ff]* ) freebsd_install  ; break;;
#                [Mm]* ) macos_install    ; break;;
                [Oo]* ) ostype="openbsd" ; getroot="doas" ; getnet="ftp -o" ; hashcheck="sha512" ; break;;
#                [Rr]* ) rasbian_install  ; break;;
#                [Uu]* ) ubuntu_install   ; break;;
	        * ) echo 'Please select which Operating System you are using';;
	    esac
	done

	# source_fetch() handles all install vs update logic
	source_fetch

	# updates go deps
	printf "\n\nUpdating Golang Dependencies...\n\n"
	cd /home/_diogenes/go && "$getroot" -u _diogenes go get -v -u github.com/spf13/viper || go_get_abort && printf "\n\nSuccessfully Updated DiogenesCCTV!\n\n"
}

#####################################################################
# Plugin Installer
#####################################################################

plugin_install () {
	echo "Plugin installer functionality is still being developed" ; exit 0
}

#####################################################################
# Script Main
#####################################################################

if [ "$(whoami)" != "root" ]; then
	printf "\n\nScript must be run as root\nExiting...\n\n" ; exit 1
fi

# Installer Greeting
# Install/Update/Add Plugin
while true ; do
printf "\nWelcome to DiogenesCCTV!\n"
printf "\nPlease Select an Option:\n"
printf "[I]nstall, [U]pdate, or [A]dd Plugin?\n\n"
printf "Selection? : "
    read -r action
    case $action in
        [Ii]* ) os_choice ; break;;
        [Uu]* ) update ; break;;
        [Aa]* ) plugin_install ; break;;
        * ) echo 'Please choose from one of the options:';;
    esac
done
